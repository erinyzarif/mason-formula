import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;


public class Mason_Formula {

	private JFrame frame;
	private JTextField num_nodes;
	private JTextField from;
	private JTextField to;
	private JTextField path_gain;
	private JTextField num_path;
	JLabel res;
	private int[] arr;
	private String[][] arrg; 
	private boolean[][] arrp;
	private String[] fp;
	private String[] fpn;
	private String[] FP;
	private String[][] loop;
	private String[] gain;
	int g = 0; 
	int f = 0;
	int l = 0;
	int gain0 = 0;
	int gain2 = 0;
	private String[] gain1;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mason_Formula window = new Mason_Formula();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Mason_Formula() {
		initialize();
	}

	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 512, 441);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Enter number of nodes :");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(53, 39, 233, 32);
		frame.getContentPane().add(lblNewLabel);
		
		final JPanel panel = new JPanel();
		panel.setBounds(10, 230, 478, 100);
		frame.getContentPane().add(panel);
		
		num_nodes = new JTextField();
		num_nodes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Graphics g = panel.getGraphics();
				arr = new int[Integer.parseInt(num_nodes.getText())];
				arrp = new boolean[Integer.parseInt(num_nodes.getText())][Integer.parseInt(num_nodes.getText())];
				int j=400/Integer.parseInt(num_nodes.getText()); int w=j;
				for(int i=0;i<Integer.parseInt(num_nodes.getText()); i++) {
					g.drawOval(w,50, 10, 10);
					arr[i] = w;
					w+=j;
				}
			} 
		});
		num_nodes.setBounds(333, 44, 96, 32);
		frame.getContentPane().add(num_nodes);
		num_nodes.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Draw path from node ");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(33, 137, 205, 25);
		frame.getContentPane().add(lblNewLabel_1);
		
		from = new JTextField();
		from.setBounds(242, 138, 81, 32);
		frame.getContentPane().add(from);
		from.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("to ");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_2.setBounds(333, 137, 24, 32);
		frame.getContentPane().add(lblNewLabel_2);
		
		to = new JTextField();
		to.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*if(arrg[0][arrg.length-1]!=null) { 
					JOptionPane.showMessageDialog(frame, "You have entered all pathes");
				}
				if((Integer.parseInt(to.getText())<arr.length)||(Integer.parseInt(from.getText())<arr.length)) {
					JOptionPane.showMessageDialog(frame, "Please enter right numer of node");					
				}*/
				Graphics g = panel.getGraphics();
				int x1 = arr[Integer.parseInt(from.getText())-1];
				int x2 = arr[Integer.parseInt(to.getText())-1];
				if(Math.abs(Integer.parseInt(to.getText())-Integer.parseInt(from.getText()))==1) {
					if(!arrp[Integer.parseInt(to.getText())-1][Integer.parseInt(from.getText())-1]) {
						g.drawLine(x1, 50, x2, 50);
					}
					else { g.drawLine(x1, 60, x2, 60); }
				}
				else if(x1-x2==0) {
					//g.drawOval(x1, 23, 30, 30);
					g.drawArc(x1, 27, 10, 60, 0, 180);
				}
				else {
					int x3 = Math.abs(x2-x1)+10;
					if(x1<x2) {
						g.drawArc(x1, 27, x3, 60, 0, 180);
					}
					else {
						g.drawArc(x2, 27, x3, 60, 0, -180);
					}
				}
				arrp[Integer.parseInt(from.getText())-1][Integer.parseInt(to.getText())-1]=true;
			}
		});
		to.setColumns(10);
		to.setBounds(369, 138, 81, 32);
		frame.getContentPane().add(to);
		
		JButton Result = new JButton("Result");
		Result.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				findForwPath(); gain0=0; findLoop();
				String[] NTL = new String[loop.length]; int ntl=0;
				for(int i=0; i<loop.length && loop[i][1] != null ; i++) {
					for(int j=i; j<loop.length && loop[j][1] != null ;j++) {
						/*if(loop[i][0].charAt(loop[i][0].length()-1) < loop[j][0].charAt(0)) {
							NTL[ntl++]=loop[i][1].concat(loop[j][1]);
						}*/
						boolean flag = true;
						for(int k=0; k<loop[i][0].length(); k++) {
							if(loop[j][0].contains(Character.toString(loop[i][0].charAt(k)))) { flag = false; }
						}
						if(flag) {
							NTL[ntl++]=loop[i][1].concat(loop[j][1]);
						}
					}
				}
				String d="1";
				for(int i=0; i<loop.length && loop[i][1]!=null; i++) {
					d = d+"-("+loop[i][1]+")";
				}
				for(int i=0; i<NTL.length && NTL[i]!=null; i++) {
					d = d+"+("+NTL[i]+")";
				}
				String[] delta = new String[fp.length];
				for(int i=0; i<fp.length && fp[i]!=null; i++) {
					delta[i] = "1";
					for(int j=0; j<loop.length && loop[j][1] != null; j++) {
						boolean flag = true;
						for(int k=0; k<fpn[i].length()&&flag; k++) {
							if(loop[j][0].contains(Character.toString(fpn[i].charAt(k)))) { flag = false; }
						}
						if(flag) {
							delta[i] = delta[i]+"-("+loop[j][1]+")";
						}
						/*if(!fp[i].contains(loop[j][0])) {
							delta[i] = delta[i]+"-("+loop[j][1]+")";
						}*/
					}
				}
				for(int i=0; i<delta.length && delta[i]!=null; i++) {	
					System.out.println(delta[i]);
				} 
				String result = "";
				for(int i=0; i<fp.length && fp[i]!=null; i++) {	
					result = result.concat(fp[i]).concat("*(").concat(delta[i]).concat(")");
				} result = result.concat("*[1/").concat(d).concat("]");
				System.out.println(result);
				res.setText(result);
				JOptionPane.showMessageDialog(null, result);
			}
		});
		Result.setFont(new Font("Tahoma", Font.PLAIN, 20));
		Result.setBounds(20, 344, 96, 32);
		frame.getContentPane().add(Result);
		
		JLabel lblPathGain = new JLabel("path gain");
		lblPathGain.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblPathGain.setBounds(85, 187, 96, 28);
		frame.getContentPane().add(lblPathGain);
		
		path_gain = new JTextField();
		path_gain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = from.getText().concat(to.getText());
				arrg[g][0] = s;  arrg[g][1]=path_gain.getText();  g++;
			}
		});
		path_gain.setFont(new Font("Tahoma", Font.PLAIN, 15));
		path_gain.setColumns(10);
		path_gain.setBounds(203, 188, 120, 32);
		frame.getContentPane().add(path_gain);
		
		JLabel lblNewLabel_3 = new JLabel("Number of paths");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_3.setBounds(63, 81, 233, 32);
		frame.getContentPane().add(lblNewLabel_3);
		
		num_path = new JTextField();
		num_path.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				arrg = new String[Integer.parseInt(num_path.getText())][2];
				fp = new String[Integer.parseInt(num_path.getText())];
				loop = new String[Integer.parseInt(num_path.getText())][2];
				gain = new String[loop.length];
				gain1 = new String[loop.length];
				FP = new String[loop.length];
				fpn = new String[loop.length];				
				for(int i=0; i<FP.length; i++) { FP[i]=""; }
			}
		});
		num_path.setColumns(10);
		num_path.setBounds(333, 86, 96, 32);
		frame.getContentPane().add(num_path);
		
		res = new JLabel("");
		res.setBounds(126, 340, 362, 54);
		frame.getContentPane().add(res);
			
	}
	private void findLoop(){
		for(int i=0;i<arr.length; i++) {
			if(arrp[i][i]) {  
				String s = Integer.toString(i).concat(Integer.toString(i));
				loop[l][0]=s; loop[l++][1]=gain(i,i);
			}
			for(int j=arr.length-1; j>i; j--) {
				gain0=0; gain2=0;
				for(int k=0; k<FP.length; k++) { FP[k]=""; } 
				for(int k=0; k<gain.length; k++) {gain[k]=null;}
				for(int k=0; k<gain1.length; k++) {gain1[k]=null;}
				String st1 = checkPath(i,j,-1); int h=0;
				for(int k=0; k<gain.length&&gain[k]!=null; k++) {h=k;}
				String st2 = checkPath(j,i,-1);
				for(int k=h+1;k<gain.length&&gain[k]!=null;k++) {
					gain1[gain2++] = gain[k];
				}
				if(st1 != null && st2 != null) {
					for(int k=0; k<gain1.length&&gain1[k]!=null; k++) {
						for(int b=0; b<h+1&&gain[b]!=null; b++) {
							loop[l][0] = FP[b];
							loop[l++][1] = gain[b].concat(gain1[k]);							
						}
						
					}
				}
				
			}
		}
	}
		
	private void findForwPath() {
		if(arrp[0][arr.length-1]) {
			fp[f++]=gain(0,arrp.length-1);
		}
		for(int i=1; i<arr.length; i++) {
			if(arrp[0][i]) {
				checkPath(i,arr.length-1,-1);
				for(int j=0; j<gain.length&&gain[j]!=null; j++) {
					boolean flag = true;
					for(int k=0; k<f; k++) {
						if(gain[j].concat(gain(0,i)).equals(fp[k])) { flag = false; }
					}
					if(flag) {fp[f++]=gain[j].concat(gain(0,i)); }
				}
			}
		}
		for(int j=0; j<FP.length&&FP[j]!=""; j++) {
			fpn[j]=FP[j];
			System.out.println(fpn[j]);
		}
	}
	
	private String checkPath(int x,int y,int z) {
		String st=null;
		if(arrp[x][y]) {
			st = gain(x,y);
			gain[gain0++]=st; 
			//FP[gain0-1] = FP[gain0-1].concat(Integer.toString(y+1).concat(Integer.toString(x+1)).concat(Integer.toString(z+1))) ;
			if(z!=-1){
				gain[gain0-1]=gain[gain0-1].concat(gain(z,x));
				//FP[gain0-1] = Integer.toString(y+1).concat(Integer.toString(x+1)).concat(Integer.toString(z+1));
			}
			if(!FP[gain0-1].contains(Integer.toString(x+1))) {
				FP[gain0-1] = FP[gain0-1].concat(Integer.toString(x+1)); 
			}
			if(!FP[gain0-1].contains(Integer.toString(y+1))) {
				FP[gain0-1] = FP[gain0-1].concat(Integer.toString(y+1)); 
			}
			if(!FP[gain0-1].contains(Integer.toString(z+1))&&z!=-1) {
				FP[gain0-1] = FP[gain0-1].concat(Integer.toString(z+1)); 
			}
			if(!FP[gain0-1].contains(Integer.toString(z+2))&&z==-1) {
				FP[gain0-1] = FP[gain0-1].concat(Integer.toString(z+2)); 
			}
		}
		for(int i=x+1; i<arrp[x].length && i<y ; i++) {
			if(arrp[x][i]) {
				st = checkPath(i,y,x);
				if(st!=null) {
					if(!gain[gain0-1].contains(gain(x,i))) {
						gain[gain0-1] = gain[gain0-1].concat(gain(x,i));
					}
					if(!FP[gain0-1].contains(Integer.toString(x+1))) {
						FP[gain0-1] = FP[gain0-1].concat(Integer.toString(x+1)); 
					}
					if(!FP[gain0-1].contains(Integer.toString(y+1))) {
						FP[gain0-1] = FP[gain0-1].concat(Integer.toString(y+1)); 
					}
					if(!FP[gain0-1].contains(Integer.toString(i+1))) {
						FP[gain0-1] = FP[gain0-1].concat(Integer.toString(i+1)); 
					}
				}
			}
		}
		return st;
	}
	
	private String gain(int x,int y) {
		String s = Integer.toString(x+1).concat(Integer.toString(y+1));
		for(int i=0; i<arrg.length; i++) {
			if(arrg[i][0].equals(s)) {
				return arrg[i][1];
			}
		}return null;
	}
}
